package hida.container;

import hida.data.Data;
import hida.data.Header;



abstract public class Container {
	
	
	private int size; //size in bytes
	
	public int getSize() {
		return size;
	}

	protected void setSize(int size) {
		this.size = size;
	}
	
	private int segmentSize=2;
	
	public int getSegmentSize() {
		return segmentSize;
	}

	public void setSegmentSize(int newSegmentSize) {
		if (newSegmentSize<=8&&newSegmentSize>0){
			this.segmentSize = newSegmentSize;
		}
		
	}

	//1-1
	private Data data;
	
	public void setData(Data newData) {
		Data oldData = this.data;
		if( newData!=oldData){
			if(oldData != null){
				this.data = null;
				oldData.setContainer(null);
			}
			this.data = newData;
			if (newData != null){
				newData.setContainer(this);
			}
		}
	}
	
	public Data getData() {
		return data;
	}
	
	//1-1
	private Header header;

	public Header getHeader(){
		return header;
	}

	public void setHeader(Header newHeader) {
		Header oldHeader = this.header;
		if( newHeader!=oldHeader){
			if(oldHeader != null){
				this.header = null;
				oldHeader.setContainer(null);
			}
			this.header = newHeader;
			if (newHeader != null){
				newHeader.setContainer(this);
			}
		}
	}
	

	
	abstract boolean insert(Data data);
	
		
	
	
	
	
}
