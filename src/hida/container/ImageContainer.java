package hida.container;


import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import hida.data.Data;
import hida.data.Header;
import hida.util.Segmentation;

public class ImageContainer extends Container {
	
	private String resultPath;
	
	public String getResultPath() {
		return resultPath;
	}

	public void setResultPath(String resultPath) {
		this.resultPath = resultPath;
	}
	
	//----------------------------
	
	private String path;

	public String getPath() {
		return path;
	}

	public void setPath(String path) throws IOException {
		this.path = path;
		checkImageAndCalcSize();
	}
	
	//----------------------------
	
	private ArrayList<Byte> segments;
	
	public ArrayList<Byte> getSegments() {
		return segments;
	}
	
	private void setSegments(ArrayList<Byte> segments) {
		this.segments = segments;
	}

	//----------------------------
	
	private int colorDepth=8; //atm static, fix it to get real depth
	
	
	
	//constructor
	public ImageContainer(){
		
	}
	
	public ImageContainer(String newPath) throws IOException{
		setPath(newPath);
		
	}
	
	public ImageContainer(String newPath, int newSegmentSize) throws IOException{
		setSegmentSize(newSegmentSize);
		setPath(newPath);
	}
	
	//----------------------------
	
	private void checkImageAndCalcSize() throws IOException{
		BufferedImage inputImage;
		inputImage = ImageIO.read(new File(getPath()));
		setSize(inputImage.getHeight()*inputImage.getWidth());
		 
	}
	
	//----------------------------
	
	@Override
	public boolean insert(Data data) {
		//if data has missing attributes, abort
		if(!data.check()){
			return false;
		}
		// setze 3er Beziehung
		setData(data);
		setHeader(new Header());
		getData().setHeader(getHeader());
		
		//create HeaderBytes
		getHeader().createHeaderBytes();
		
		//check container and start insert
		if(check()){
			try {
				
				//decode and encode image
				BufferedImage inputImage = ImageIO
						.read(new File(getPath()));
	
				BufferedImage outputImage = new BufferedImage(
						inputImage.getWidth(), inputImage.getHeight(),
						BufferedImage.TYPE_INT_RGB);
				
				for (int x = 0; x < inputImage.getWidth(); x++) {
					for (int y = 0; y < inputImage.getHeight(); y++) {
	
						int rgb = inputImage.getRGB(x, y);
						outputImage.setRGB(x, y, rgb);
					}
				}
				ImageIO.write(outputImage, "png", new File(getResultPath()));
				
				//decode image
				inputImage = ImageIO.read(new File(getResultPath()));
				outputImage = new BufferedImage(
						inputImage.getWidth(), inputImage.getHeight(),
						BufferedImage.TYPE_INT_RGB);
				
				outputImage = new BufferedImage(inputImage.getWidth(),
						inputImage.getHeight(), BufferedImage.TYPE_INT_RGB);
	
				int index=0;
				
				//erstelle arraylist
				ArrayList<Byte> headerAndData = new ArrayList<Byte>();
				byte[] b=getData().getHeader().getBytes();
				for(int i=0; i<getData().getHeader().getSize(); i++){
					headerAndData.add(b[i]);
				}
				b=getData().getBytes();
				for(int i=0; i<getData().getSize(); i++){
					headerAndData.add(b[i]);
				}
				// erstelle segmente
				setSegments(Segmentation.splitIntoSegments(headerAndData, getSegmentSize()));
				
				if(getSegments()==null){
					return false;
				}
				//erstelle mask
				byte mask =-1;
				for(int j=0; j<getSegmentSize(); j++){
					mask = (byte) ((mask<<1) & 0xFF);
				}
	
				for (int x = 0; x < inputImage.getWidth(); x++) {
					for (int y = 0; y < inputImage.getHeight(); y++) {
	
						int rgb = inputImage.getRGB(x, y);
						int blue = 0x0000ff & rgb;

						int green = 0x0000ff & (rgb >> 8);

						int red = 0x0000ff & (rgb >> 16);


						
						//insert data
						if(index<getSegments().size()){
							blue = blue & (mask & 0xFF);
							blue =( blue & 0xFF) | (getSegments().get(index) & 0xFF);
							index++;
						}
						if(index<getSegments().size()){
							green = green & (mask & 0xFF);
							green =( green& 0xFF) | (getSegments().get(index) & 0xFF);
							index++;
						}
						if(index<getSegments().size()){
							red = red & (mask & 0xFF);
							red = (red& 0xFF) | (getSegments().get(index) & 0xFF);
							index++;
						}
						//System.out.println(blue);
						//System.out.println(green);
						//System.out.println(red);
						int newRgb = blue | (green << 8) | (red << 16);
	
						outputImage.setRGB(x, y, newRgb);
					}
				}
				ImageIO.write(outputImage, "png", new File(getResultPath()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		
		}
		else{
			return false;
		}
		return true;
	}
	

	private void makeSegments() {
		
		//nonsense
		/*
		byte[] b = new byte[4*getData().getHeader().getSize()+(getData().getSize()*8)/getSegmentSize()+1];
		int bytesIndex=0;
		int bitIndex=0;
		
		byte[] h = getData().getHeader().getBytes();
		byte[] d = getData().getBytes();
		
		for(int i = 0; i< getData().getHeader().getSize(); i++){
			
			b[bytesIndex] = (byte) (h[i]>>>6);
			bytesIndex++;
			b[bytesIndex] = (byte) ((h[i]>>>4)& 0b00000011);
			bytesIndex++;
			b[bytesIndex] = (byte) ((h[i]>>>2)& 0b00000011);
			bytesIndex++;
			b[bytesIndex] = (byte) (h[i]& 0b00000011);
			bytesIndex++;
		}
		
		for(int i=0; i<getData().getSize(); i++){
			b[bytesIndex] = (byte) (h[i]>>>8-getSegmentSize());
			bytesIndex++;
			
		}
	
		byte[] bytes = new byte[b.length + d.length];
		int totalWrittenBits = 0;
		int writtenBits = 0;
		int rgbTracker = 0;
		
		for (int i = 0; i < b.length; i++) {
			bytes[i] = b[i];
		}
		for (int i = 0; i < d.length; i++) {
			bytes[i + b.length] = d[i];
		}*/
	}

	public boolean test1(Data data){
		//total nonsense
		BufferedImage bi;	
		/*
		try {
		
			bi = ImageIO.read(new File("C:\\HiDa\\2.jpeg"));
			System.out.println(bi.getHeight());
			System.out.println(bi.getColorModel().toString());
			System.out.println(bi.getRGB(0, 0));
			for(int x=0; x<bi.getWidth();x++){
				//int temp=bi.getRGB(x, 0);
				
				bi.setRGB(x, 1, 0b00000000_00000000_00000000_11111111);
				bi.setRGB(x, 2, 0b00000000_00000000_11111111_11111111);
				bi.setRGB(x, 3, 0b00000000_11111111_11111111_11111111);
				bi.setRGB(x, 4, 0b00000000_00000000_00000000_00000000);
				
				bi.setRGB(x, 5, 0b11111111_00000000_00000000_11111111);
				bi.setRGB(x, 6, 0b11111111_00000000_11111111_00000000);
				bi.setRGB(x, 7, 0b11111111_11111111_00000000_00000000);
				bi.setRGB(x, 8, 0b11111111_00000000_00000000_00000000);
				bi.setRGB(x, 9, 0b00000000_00000000_00000000_00000000);
				bi.setRGB(x, 10, 0b0000000_00000000_01111111_00000000);
				bi.setRGB(x, 11, 0b11111111_01111111_00000000_00000000);
				bi.setRGB(x, 12, 0b11111111_11111111_11111111_11111111);
			}
			
			ImageIO.write(bi,"jpeg",new File("c:\\HiDa\\2a.jpeg"));
			bi = ImageIO.read(new File("C:\\HiDa\\2a.jpeg"));
			for(int x=0; x<bi.getWidth();x++){
				//int temp=bi.getRGB(x, 0);
				
				bi.setRGB(x, 1, 100);
				bi.setRGB(x, 2, 255);
				bi.setRGB(x, 3, 15647);
				bi.setRGB(x, 4, 78675200);
				
				bi.setRGB(x, 5, 1);
				bi.setRGB(x, 6, 0);
				bi.setRGB(x, 7, 0b11111111_11111111_00000000_00000000);
				bi.setRGB(x, 8, 0b11111111_00000000_00000000_00000000);
				bi.setRGB(x, 9, 0b00000000_00000000_00000000_00000000);
				bi.setRGB(x, 10, 0b0000000_00000000_01111111_00000000);
				bi.setRGB(x, 11, 0b11111111_01111111_00000000_00000000);
				bi.setRGB(x, 12, 0b11111111_11111111_11111111_11111111);
			}
			ImageIO.write(bi,"jpeg",new File("c:\\HiDa\\test02.jpeg"));
			bi = ImageIO.read(new File("C:\\HiDa\\test02.jpeg"));
			System.out.println(bi.getRGB(0, 1));
			System.out.println(bi.getRGB(0, 2));
			System.out.println(bi.getRGB(0, 3));
			System.out.println(bi.getRGB(0, 4));
			System.out.println(bi.getRGB(0, 5));
			System.out.println(bi.getRGB(0, 6));
	/*		for (int i=0; i<100;i++){
				bi = ImageIO.read(new File("C:\\HiDa\\2a.jpeg"));
				Thread.sleep(500);
				ImageIO.write(bi,"jpeg",new File("c:\\HiDa\\2a.jpeg"));
			}
			
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
		//into greeaeyscale
        try {
            BufferedImage inputImage = ImageIO.read(new File("C:\\HiDa\\2.jpeg"));

            BufferedImage outputImage = new BufferedImage(
                    inputImage.getWidth(), inputImage.getHeight(),
                    BufferedImage.TYPE_INT_RGB);
            for (int x = 0; x < inputImage.getWidth(); x++) {
                for (int y = 0; y < inputImage.getHeight(); y++) {
                    int rgb = inputImage.getRGB(x, y);
                    int blue = 0x0000ff & rgb;
                    
                    int green = 0x0000ff & (rgb >> 8);
                   
                    int red = 0x0000ff & (rgb >> 16);
                    

                    int lum = (int) (red * 0.299 + green * 0.587 + blue * 0.114);
                    outputImage
                            .setRGB(x, y, lum | (lum << 8) | (lum << 16));
                }
            }
            ImageIO.write(outputImage, "jpg", new File("C:\\HiDa\\2gray.jpeg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        */
        try {
            BufferedImage inputImage = ImageIO.read(new File("C:\\HiDa\\3x.jpg"));

            BufferedImage outputImage = new BufferedImage(
                    inputImage.getWidth(), inputImage.getHeight(),
                    BufferedImage.TYPE_INT_RGB);
            for (int x = 0; x < inputImage.getWidth(); x++) {
                for (int y = 0; y < inputImage.getHeight(); y++) {

                    int rgb = inputImage.getRGB(x, y);
                    int blue=0;
                    blue = 0x0000ff & rgb;
                    int green=0;
                    green = 0x0000ff & (rgb >> 8);
                    int red =0;
                    red= 0x0000ff & (rgb >> 16);
                    if(x==0 && y==0){
                    	System.out.println(0x00ffffff & rgb);
                    	System.out.println(blue);
                    	System.out.println(green);
                    	System.out.println(red); 
                    	
        
                    //	newRgb = newRgb & (green<<8);
                    //	newRgb = newRgb & (blue<<16);

                    }
                    
     
                    
                  int newRgb = blue | (green << 8) | (red << 16);
        
                    int lum = (int) (red * 0.299 + green * 0.587 + blue * 0.114);
                    outputImage.setRGB(x, y, newRgb);
                }
            }
            ImageIO.write(outputImage, "jpg", new File("C:\\HiDa\\3x.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        try {
            BufferedImage inputImage = ImageIO.read(new File("C:\\HiDa\\3x.jpg"));

            for (int x = 0; x < inputImage.getWidth(); x++) {
                for (int y = 0; y < inputImage.getHeight(); y++) {
                    int rgb = inputImage.getRGB(x, y);
                    int blue=0;
                    blue = 0x0000ff & rgb;
                    int green=0;
                    green = 0x0000ff & (rgb >> 8);
                    int red =0;
                    red= 0x0000ff & (rgb >> 16);
                    if(x==0 && y==0){
                    	System.out.println(0x00ffffff & rgb);
                    	System.out.println(blue);
                    	System.out.println(green);
                    	System.out.println(red); 
                    }	
        
                    //	newRgb = newRgb & (green<<8);
                    //	newRgb = newRgb & (blue<<16);
                    blue=(byte)60;
                    red=70;
                    green=80;
                    int newRgb = blue | (green << 8) | (red << 16);
                    
                    int lum = (int) (red * 0.299 + green * 0.587 + blue * 0.114);
                    inputImage.setRGB(x, y, newRgb);
                }
            }
            ImageIO.write(inputImage, "jpg", new File("C:\\HiDa\\3x.jpg"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
		
        try {
            BufferedImage inputImage = ImageIO.read(new File("C:\\HiDa\\3x.jpg"));

            for (int x = 0; x < inputImage.getWidth(); x++) {
                for (int y = 0; y < inputImage.getHeight(); y++) {
                    int rgb = inputImage.getRGB(x, y);
                    int blue=0;
                    blue = 0x0000ff & rgb;
                    int green=0;
                    green = 0x0000ff & (rgb >> 8);
                    int red =0;
                    red= 0x0000ff & (rgb >> 16);
                   
                    	System.out.println(0x00ffffff & rgb);
                    	System.out.println(blue);
                    	System.out.println(green);
                    	System.out.println(red); 
       
        
                    //	newRgb = newRgb & (green<<8);
                    //	newRgb = newRgb & (blue<<16);

                    
                }
            }
                } catch (IOException e) {
                    e.printStackTrace();
                }
		/*WritableImage img = new WritableImage(getPath());

		PixelWriter pw = img.get
		pr.getArgb(0, 0);*/
		return false;
	}

	//----------------------------

	boolean check() {
		if (getData()!=null && getPath()!=null && getResultPath() !=null){
			return true;
		}
		else{
			return false;
		}
	}

	public void compareImage() throws IOException {
        BufferedImage inputImage = ImageIO.read(new File(getPath()));

        BufferedImage outputImage = ImageIO.read(new File(getResultPath()));
        for (int x = 0; x < inputImage.getWidth(); x++) {
            for (int y = 0; y < inputImage.getHeight(); y++) {

                int rgb = inputImage.getRGB(x, y);
                int blue=0;
                blue = 0x0000ff & rgb;
                int green=0;
                green = 0x0000ff & (rgb >> 8);
                int red =0;
                red= 0x0000ff & (rgb >> 16);
                if(x==0 && y<8){
                	System.out.println("----" + x + "--" + y);
                	System.out.println("srcI: " + (0x00ffffff & rgb));
                	System.out.println("srcI: " +blue);
                	System.out.println("srcI: " +green);
                	System.out.println("srcI: " +red); 
                }
                
                rgb = outputImage.getRGB(x, y);
                blue=0;
                blue = 0x0000ff & rgb;
                green=0;
                green = 0x0000ff & (rgb >> 8);
                red =0;
                red= 0x0000ff & (rgb >> 16);
                if(x==0 && y<8){
                	System.out.println(0x00ffffff & rgb);
                	System.out.println(blue);
                	System.out.println(green);
                	System.out.println(red); 
                }
                if(x==0 && y<8){
                	System.out.println("segment "+y*3+" : "+getSegments().get(y*3)+(y*3+1)+" : "+getSegments().get(y*3+1)+(y*3+2)+" : "+getSegments().get(y*3+2));
                }
            }
        }
	}
}
