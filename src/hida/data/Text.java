package hida.data;

public class Text extends Data{

	/*valid CharSets: 
	US-ASCII 	Seven-bit ASCII, a.k.a. ISO646-US, a.k.a. the Basic Latin block of the Unicode character set
	ISO-8859-1  ISO Latin Alphabet No. 1, a.k.a. ISO-LATIN-1
	UTF-8 	    Eight-bit UCS Transformation Format
	UTF-16BE 	Sixteen-bit UCS Transformation Format, big-endian byte order
	UTF-16LE 	Sixteen-bit UCS Transformation Format, little-endian byte order
	UTF-16		Sixteen-bit UCS Transformation Format, byte order identified by an optional byte-order mark
	*/
	public static final String US_ASCII = "US-ASCII";  //default
	public static final String ISO_8859_1 = "ISO-8859-1";
	public static final String UTF_8 = "UTF-8";
	public static final String UTF_16BE = "UTF-16BE";
	public static final String UTF_16LE = "UTF-16LE";
	public static final String UTF_16 = "UTF-16";
	public static final String DEFAULT_CHARSET = US_ASCII; // for test purpose
	
	//attributes
	String value;
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
		encode();
	}
	
	//-----
	private String charSet = DEFAULT_CHARSET;
	
	public String getCharSet() {
		return charSet;
	}

	public void setCharSet(String newCharSet) {
		
		if(newCharSet.equals(US_ASCII)
				|| newCharSet.equals(ISO_8859_1)
				|| newCharSet.equals(UTF_8)
				|| newCharSet.equals(UTF_16BE)
				|| newCharSet.equals(UTF_16LE)
				|| newCharSet.equals(UTF_16))
		{
			this.charSet = newCharSet;
		}
		else{
			//FIX: throw something
		}
		
	}
	
	
	
	//Constructor
	
	public Text(){
	}
	
	public Text(String newValue){
		setValue(newValue);
	}
	
	public Text(String newValue, String charSet){
		setCharSet(charSet);
		setValue(newValue);
	}
	
	
	//methods
	

	@Override
	public boolean check() {
		if(getValue()==null){
			//missing value
			return false;
		}
		//if value has been set, bytes[] and size is also set

		return true;
	}
	
	
	/**
	 * String -> Byte[]. 
	 * Sets the byte attribute, using the setValue()
	 * For default ASCII encoding only
	 */
	private void encode() {
		char[] charArray = getValue().toCharArray();
		byte[] ba = new byte[charArray.length];
		for(int i = 0; i<charArray.length; i++){
			ba[i] = (byte)charArray[i];
		}
		setBytes(ba); 
	}

}
