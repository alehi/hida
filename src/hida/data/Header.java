package hida.data;

import hida.container.Container;
import hida.container.ImageCollectionContainer;
import hida.container.ImageContainer;

/*
 * Header aufbau
 *   |   0	|   1	|   2	|   3	 |   4	 |   5	|   6	 |   7   |
 *   +-------------------------------+-------------------------------+  
 * 0 |	         DATA-TYPE           |        CONTAINER-TYPE         |
 *   +-------------------------------+-------+-----------------------+
 * 1 | 			-empty-    			         |     SEGMENT-SIZE      |
 *   +---------------------------------------+-----------------------+
 * 2 |						   DATA-BYTES-SIZE						 |
 *   +---------------------------------------------------------------+
 * 3 |   					   DATA-BYTES-SIZE                       |
 * 	 +---------------------------------------------------------------+
 * 4 |  					   DATA-BYTES-SIZE                       |
 * 	 +---------------------------------------------------------------+ 
 * 5 |						   DATA-BYTES-SIZE                       |
 * 	 +---------------------------------------------------------------+
 * 6 |						DATA-DEPENDENT PARA1                     |
 * 	 +---------------------------------------------------------------+
 * 7 |					 CONTAINER-DEPENDENT PARA1                   |
 * 	 +---------------------------------------------------------------+
 * 
 * DATA-TYP: 4-Bit
 * 			 0000 default(invalid)
 * 			 0001 for Text
 * 			 0010 for TextFile
 * 			 0011 for ImageFile
 * 
 * CONTAINER-TYPE: 4-Bit
 * 			 0000 default(invalid)
 * 			 0001 for single ImageContainer
 * 			 0010 for ImageCollectionContainer
 * 
 * SEGMENT-SIZE: 3 Bit
 * 			value 0-7 for SegmentSize 1-8
 * 
 * DATA-BYTES-SIZE: 0 to 2^32 -1
 * 					2_3_4_5 = 32Bit
 * 
 * DATA-DEPENDENT PARA:
 * 		For Text: 
 * 			charset
 * 			00000001 UTF-8
 * 			00000010 UTF-16
 * 
 * CONTAINER-DEPENDENT PARA:
 * 		For Image-Container:
 * 			
 * 
 * 
 */

public class Header {
	
	private int size=8;
	
	public int getSize() {
		return size;
	}
	private void setSize(int size) {
		this.size = size;
	}
	
	private byte[] bytes = new byte[size];  //value in bytes
	
	private void setBytes(byte[] bytes){
		this.bytes = bytes;
		setSize(bytes.length);
	}
	
	public byte[] getBytes(){
		return this.bytes;
	}
	

	
	//1-1
	private Data data;
	
	public void setData(Data newData) {
		Data oldData = this.data;
		if( newData!=oldData){
			if(oldData != null){
				this.data = null;
				oldData.setHeader(null);
			}
			this.data = newData;
			if (newData != null){
				newData.setHeader(this);
			}
			createHeaderBytes();
		}
	}
	
	public Data getData() {
		return data;
	}
	
	//1-1
	private Container container;
	
	public void setContainer(Container newContainer) {
		Container oldContainer = this.container;
		if( newContainer!=oldContainer){
			if(oldContainer != null){
				this.container = null;
				oldContainer.setHeader(null);
			}
			this.container = newContainer;
			if (newContainer != null){
				newContainer.setHeader(this);
			}
		}
	}
	
	public Container getContainer() {
		return container;
	}
	
			
	
	//Konstruktor
	
	public Header(){
		
	}
	
	public Header(Data newData){
		setData(newData);		
	}
	
	
	
	// write XY
	
	//erases Data-Type field and sets it to current Data object
	private boolean writeDataType(){
		if(getData() != null){
			byte[] all = getBytes();
			byte b = all[0];
		
			if(getData() instanceof Text){
				b=(byte) (b & 0b0000_1111);
				b= (byte) (b | 0b0001_0000);
			}
			if(getData() instanceof TextFile){
				b=(byte) (b & 0b0000_1111);
				b= (byte) (b | 0b0010_0000);
			}
			if(getData() instanceof ImageFile){
				b=(byte) (b & 0b0000_1111);
				b= (byte) (b | 0b0011_0000);
			}
			all[0]=b;
			setBytes(all);
			return true;
		}
		else{
			return false;
		}
	}
	
	//erases Container-Type field and sets it to current Container object
	private boolean writeContainerType(){
		if(getContainer() != null){
			byte[] all = getBytes();
			byte b = all[0];
		
			if(getContainer() instanceof ImageContainer){
				b=(byte) (b & 0b1111_0000);
				b= (byte) (b | 0b0000_0001);
			}
			if(getContainer() instanceof ImageCollectionContainer){
				b=(byte) (b & 0b1111_0000);
				b= (byte) (b | 0b0000_0010);
			}

			all[0]=b;
			setBytes(all);
			return true;
		}
		else{
			return false;
		}
	}
	
	private boolean writeDataBytesSize(){
		if(getData().getSize() != 0){
			byte[] all = getBytes();
			
			int itemp;
			int size=data.getSize();
			itemp = size& 0b11111111_00000000_00000000_00000000;
			itemp = itemp/0b00000000_11111111_11111111_11111111;
			all[2] = (byte)itemp;
			itemp = size& 0b00000000_11111111_00000000_00000000;
			itemp = itemp/0b00000000_00000000_11111111_11111111;
			all[3] = (byte)itemp;
			itemp = size& 0b00000000_00000000_11111111_00000000;
			itemp = itemp/0b00000000_00000000_00000000_11111111;
			all[4] = (byte)itemp;
			itemp = size& 0b00000000_00000000_00000000_11111111;
			all[5] = (byte)itemp;	
		
			setBytes(all);
			return true;
		}
		else{
			return false;
		}
	}
	
	private boolean writeSegmentSize(){
		if(getContainer().getSegmentSize() <9 && getContainer().getSegmentSize() >0){
			byte[] all = getBytes();
			byte b = all[1];
		
			b=(byte) (b & 0b1111_1000);
			b= (byte) (b | (getContainer().getSegmentSize()-1));

			all[1]=b;
			setBytes(all);
			return true;
		}
		else{
			return false;
		}
	}
	
	/**
	 * Creates a individual header
	 * @return true if successful, false if not
	 */
	public boolean createHeaderBytes() {
		if(getData() !=null && getContainer() != null){
			
			//sets every bytes field to zero
			byte[] b = new byte[8];
			for (int i=0; i<8; i++){
				b[i]=0b00000000;
			}
			setBytes(b);
			
			//write bytes
			if(!writeDataType()){
				//resetBytes
				setBytes(b);
				return false;
			}
			if(!writeContainerType()){
				//resetBytes
				setBytes(b);
				return false;
			}
			if(!writeSegmentSize()){
				//resetBytes
				setBytes(b);
				return false;
			}
			if(!writeDataBytesSize()){
				//resetBytes
				setBytes(b);
				return false;
			}
			return true;
		
		}
		else{
			return false;
		}
		
		
	}
	
	
	
}
