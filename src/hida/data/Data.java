package hida.data;

import hida.container.Container;

abstract public class Data {
	
	protected int size; //size of value in bytes
	
	public int getSize() {
		return size;
	}

	private void setSize(int size) {
		this.size = size;
	}
	
	
	private byte[] bytes;  //value in bytes
	
	protected void setBytes(byte[] bytes){
		this.bytes = bytes;
		setSize(bytes.length);
	}
	
	public byte[] getBytes(){
		return this.bytes;
	}
	
	//1-1
	private Header header;

	public Header getHeader(){
		return header;
	}

	public void setHeader(Header newHeader) {
		Header oldHeader = this.header;
		if( newHeader!=oldHeader){
			if(oldHeader != null){
				this.header = null;
				oldHeader.setData(null);
			}
			this.header = newHeader;
			if (newHeader != null){
				newHeader.setData(this);
			}
		}
	}
	
	//1-1
	private Container container;
	
	public void setContainer(Container newContainer) {
		Container oldContainer = this.container;
		if( newContainer!=oldContainer){
			if(oldContainer != null){
				this.container = null;
				oldContainer.setData(null);
			}
			this.container = newContainer;
			if (newContainer != null){
				newContainer.setData(this);
			}
		}
	}
	
	public Container getContainer() {
		return container;
	}
	
	
	
	/**Checks if everything necessary is set, and sets missing attributes.
	 * Creates individual Header.
	 * @return true if successful, false else
	 */
	abstract public boolean check();
	
	
}
