package hida.util;

import java.util.ArrayList;

public class Segmentation {
	
	/**
	 * Splits an ArrayList<Byte> into segments.
	 * The last Segment will be filled with 0's, if there are no more Bits available.
	 * This happens when  source.size()*8 % segmentSize !=0
	 * @param source 
	 *
	 * @param segmentSize
	 * SegmentSize in Bit. Range from 1-8.
	 * @return
	 * returns null if segmentSize<1 or segmentSize>8, or if source.size()<2 
	 */
	
	public static ArrayList<Byte> splitIntoSegments(ArrayList<Byte> source, int segmentSize){

		
		//terminates if..
		if(source.size()<2 || segmentSize<1 || segmentSize>8){
			return null;
		}
		
		
		//creates a mask for byte operations
		//depends on segmentSize. e.g. for segSize=5 -> 00011111;
		byte mask = 0;
		for(int j=0; j<segmentSize; j++){
			mask = (byte) ((mask<<1)+1);
		}
		
		//initialize 
		ArrayList<Byte> result = new ArrayList<Byte>();
		int temp=0;
		int k=0;
		int count2 = 1;
		boolean end=false;

		int i = (byte) source.get(0);
		i= i << 8;
		i= i + ( source.get(1) &0xFF);
		
		//magic starts
		while(count2<source.size()){
			temp = i >>>(16-segmentSize-k);
			result.add( (byte) (temp&mask));
			k=k+segmentSize;
			if(k>7){
				k=k%8;
				if(count2+1<source.size()){
					i = (i<<8) + (source.get(count2+1)&0xFF);
					count2++;
				}
				else if(!end){
					i = (i<<8);
					end=true;
				}
				else{
					count2++;
				}
			}
		}

		return result;
	}
	
}
