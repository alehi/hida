package test;

import java.io.IOException;
import java.util.ArrayList;

import hida.container.ImageContainer;
import hida.data.Data;
import hida.data.Text;
import hida.util.Segmentation;

public class test {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		/*
		////////////
		Text txt = new Text();
		txt.setValue("test");
		txt.makeRdy();
		////////////
		System.out.println("charset:");
		System.out.println(txt.getCharSet());
		System.out.println("value:");
		System.out.println(txt.getValue());
		System.out.println("size:");
		System.out.println(txt.getSize());
		System.out.println("bytes:");
		for(int i=0; i<txt.getSize(); i++){
			System.out.println(txt.getBytes()[i]);
		}
		System.out.println("Header.size:");
		System.out.println(txt.getHeader().getSize());
		System.out.println("Header.bytes:");
		for(int i=0; i<txt.getHeader().getSize(); i++){
			System.out.println(txt.getHeader().getBytes()[i]);
		}
		System.out.println("_______________________________");
		
		
		/////////
		Text txt = new Text("Hallo!");


		/////////
		System.out.println("charset:");
		System.out.println(txt.getCharSet());
		System.out.println("value:");
		System.out.println(txt.getValue());
		System.out.println("size:");
		System.out.println(txt.getSize());
		System.out.println("bytes:");
		for(int i=0; i<txt.getSize(); i++){
			System.out.println(txt.getBytes()[i]);
		}
	
	
		txt=new Text("blabla");

		ImageContainer ic = new ImageContainer("c:\\HiDa\\2.jpeg");
		ic.insert(txt);

		byte[] b = new byte[3];
		b[0]=0b00110100;
		b[1]=0b00011011;
		b[2]=0b01010011;
		byte[] c = new byte[b.length*8/3+1];
		
		byte mask = 0;
		
		for(int j=0; j<3; j++){
			mask = (byte) ((mask<<1)+1);
		}
		
		System.out.println(mask);
		int temp=0;
		int k=0;
		int count = 0;
		int count2 = 1;
		boolean end=false;
		
		int i = b[0];
		i= i << 8;
		i= i + b[1];
		while(count2<b.length){
			temp = i >>>(16-3-k);
			c[count] = (byte) (temp&mask);
			count++;
			k=k+3;
			if(k>7){
				k=k%8;
				if(count2+1<b.length){
					i = (i<<8) + b[count2+1];
					count2++;
				}
				else if(!end){
					i = (i<<8);
					end=true;
				}
				else{
					count2++;
				}
				
			}
		}
		
		for(int p=0; p<c.length;p++){
			System.out.println(c[p]);
		}
		
		
		//startsituation:
		ArrayList<Byte> b = new ArrayList<Byte>(20);

		System.out.println(b.size());
		b.set(1, (byte) 0b00110100);
		b.set(1, (byte) 0b00011011);
		b.set(2, (byte) 0b01010011);
		int segS = 3;
		
		//Aktion:
		ArrayList<Byte> seg = Segmentation.splitIntoSegments(b, segS);
		
		//Endsituation:
		System.out.println(seg.get(0));
		*/

		
		
		Text t = new Text();
		String s = new String("I also did a double-take when I saw a "
				+ "language designer make that quote. There is nothing "
				+ "simpler than an unsigned integer. Signed integers are complicated."
				+ " Particularly when you consider the bit twiddling at the transistor"
				+ " level. And how does a signed integer shift? I had to conclude that"
				+ " the dessigner of Java has a serious issue understanding boolean logic.");
		String p = new String(s+s+s+s+s+s+s);
		String q = new String(p+p+p+p+p+p+p+p+p+p+p);
		String k = new String(q+q+q+q+q+q+q+q+q+q+q+q); 
		
		
		t.setValue(k);
		
		ImageContainer ic = new ImageContainer();
		try {
			ic.setPath("c:\\HiDa\\3.jpg");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ic.setResultPath("c:\\HiDa\\3_8bit.png");
		ic.setSegmentSize(8);
		ic.insert(t);
		//ic.compareImage();
		/*
		ImageContainer ic = new ImageContainer();
		ic.test1(new Text());
		ic.test1(new Text());
		ic.test1(new Text());*/
	}

}
