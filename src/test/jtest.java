package test;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import hida.container.Container;
import hida.container.ImageContainer;
import hida.data.Data;
import hida.data.Header;
import hida.data.Text;
import hida.util.Segmentation;

import org.junit.Assert;
import org.junit.Test;

public class jtest {
	


	@Test
	public void ConAndSetterTextTest1() {
		//startsituation:
		//Aktion:
		Text txt = new Text();
		txt.setCharSet("UTF-16");
		txt.setValue("Hallo");
		
		//Endsituation:
		Assert.assertEquals("charset wrong value", "UTF-16", txt.getCharSet());
		Assert.assertEquals("string wrong value", "Hallo", txt.getValue());
		Assert.assertEquals("size wrong value", 5, txt.getSize());
		byte[] b={72, 97, 108, 108, 111};
		Assert.assertArrayEquals("bytes wrong value",b,txt.getBytes());	
	}
	
	@Test
	public void ConAndSetterTextTest2() {
		//startsituation:
		//Aktion:
		Text txt = new Text("Hallo");
		txt.setCharSet("UTF-16");

		
		//Endsituation:
		Assert.assertEquals("charset wrong value", "UTF-16", txt.getCharSet());
		Assert.assertEquals("string wrong value", "Hallo", txt.getValue());
		Assert.assertEquals("size wrong value", 5, txt.getSize());
		byte[] b={72, 97, 108, 108, 111};
		Assert.assertArrayEquals("bytes wrong value",b,txt.getBytes());	
	}
	
	@Test
	public void ConAndSetterTextTest3() {
		//startsituation:
		//Aktion:
		Text txt = new Text("Hallo","UTF-16");
		
		//Endsituation:
		Assert.assertEquals("charset wrong value", "UTF-16", txt.getCharSet());
		Assert.assertEquals("string wrong value", "Hallo", txt.getValue());
		Assert.assertEquals("size wrong value", 5, txt.getSize());
		byte[] b={72, 97, 108, 108, 111};
		Assert.assertArrayEquals("bytes wrong value",b,txt.getBytes());	
	}
	
	@Test
	public void ConAndSetterTextTest4() {
		//startsituation:
		//Aktion:
		Text txt = new Text("Hallo");
		
		//Endsituation:
		Assert.assertEquals("charset wrong value", Text.DEFAULT_CHARSET, txt.getCharSet());
		Assert.assertEquals("string wrong value", "Hallo", txt.getValue());
		Assert.assertEquals("size wrong value", 5, txt.getSize());
		byte[] b={72, 97, 108, 108, 111};
		Assert.assertArrayEquals("bytes wrong value",b,txt.getBytes());
	}
	
	@Test
	public void WrongCharsetTest() {
		//startsituation:
		//Aktion:
		Text txt = new Text();
		txt.setCharSet("UTF-15");
		txt.setValue("Hallo");
		
		//Endsituation:
		Assert.assertEquals("charset wrong value", Text.DEFAULT_CHARSET, txt.getCharSet());
		Assert.assertEquals("string wrong value", "Hallo", txt.getValue());
		Assert.assertEquals("size wrong value", 5, txt.getSize());
		byte[] b={72, 97, 108, 108, 111};
		Assert.assertArrayEquals("bytes wrong value",b,txt.getBytes());	
	}
	
	
	/*
	 * 
	 * IMAGECONTAINER TESTS
	 * 
	 */
	
	@Test
	public void ConAndSetterImageContainerTest1() {
		//startsituation:
		//Aktion:
		ImageContainer ic = new ImageContainer();
		
		//Endsituation:
		
		Assert.assertNull("data not null", ic.getData());
		Assert.assertNull("path not null", ic.getPath());
		Assert.assertNull("segments not null", ic.getSegments());
		Assert.assertEquals("size not 0", 0 ,ic.getSize());
		Assert.assertEquals("default SegmentSize not 2",2, ic.getSegmentSize());
	}
	
	
	
	@Test
	public void ConAndSetterImageContainerTest2() {

		try {
			// startsituation:
			// Aktion:
			ImageContainer ic;
			ic = new ImageContainer("C:\\HiDa\\1.jpg");

			// Endsituation:

			Assert.assertNull("data not null", ic.getData());
			Assert.assertEquals("path wrong value", "C:\\HiDa\\1.jpg",
					ic.getPath());
			Assert.assertNotNull("size is null", ic.getSize());
			Assert.assertNull("segments not null", ic.getSegments());
			Assert.assertEquals("default SegmentSize not 2", 2,
					ic.getSegmentSize());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void ConAndSetterImageContainerTest3() {

		try { 
			// startsituation:
			// Aktion:
			ImageContainer ic;
			ic = new ImageContainer("C:\\HiDa\\1.jpg", 5); 
			// Endsituation:

			Assert.assertNull("data not null", ic.getData());
			Assert.assertNull("segments not null", ic.getSegments());
			Assert.assertNotNull("size is null", ic.getSize());
			Assert.assertEquals("path wrong value", "C:\\HiDa\\1.jpg",
					ic.getPath());
			Assert.assertEquals("SegmentSize wrong value", 5,
					ic.getSegmentSize());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Test
	public void ConAndSetterImageContainerTest4() {

		try {
			// startsituation:
			// Aktion:
			ImageContainer ic = new ImageContainer();
			ic.setPath("C:\\HiDa\\1.jpg");
			ic.setSegmentSize(5);
			// Endsituation:

			Assert.assertNull("data not null", ic.getData());
			Assert.assertNull("segments not null", ic.getSegments());
			Assert.assertNotNull("size is null", ic.getSize());
			Assert.assertEquals("path wrong value", "C:\\HiDa\\1.jpg",
					ic.getPath());
			Assert.assertEquals("SegmentSize wrong value", 5,
					ic.getSegmentSize());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Test
	public void ConAndSetterImageContainerTest5() {
		// segmentSize too big, should stay at default: 2
		// FIX

		try { // startsituation:
			// Aktion: 
			ImageContainer ic = new ImageContainer();
			ic.setPath("C:\\HiDa\\1.jpg");
			ic.setSegmentSize(9);
			// Endsituation:

			Assert.assertNull("data not null", ic.getData());
			Assert.assertNull("segments not null", ic.getSegments());
			Assert.assertNotNull("size is null", ic.getSize());
			Assert.assertEquals("path wrong value", "C:\\HiDa\\1.jpg",
					ic.getPath());
			Assert.assertEquals("SegmentSize wrong value", 2,
					ic.getSegmentSize());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/*
	 * 
	 * SEGMENTATION-TESTS
	 * 
	 */
	
	@Test
	public void SegmentationTest1() {
		//startsituation:
		ArrayList<Byte> b = new ArrayList<Byte>();

		b.add( (byte) 0b00110100);
		b.add( (byte) 0b00011011);
		b.add( (byte) 0b01010011);
		int segS = 3;
		
		//Aktion:
		ArrayList<Byte> seg = Segmentation.splitIntoSegments(b, segS);
		
		//Endsituation:
		Assert.assertEquals("wrong value", 8, seg.size());
		Assert.assertEquals("wrong value", 0b00000_001, (byte) seg.get(0));
		Assert.assertEquals("wrong value", 0b00000_101, (byte) seg.get(1));
		Assert.assertEquals("wrong value", 0b00000_000, (byte) seg.get(2));
		Assert.assertEquals("wrong value", 0b00000_001, (byte) seg.get(3));
		Assert.assertEquals("wrong value", 0b00000_101, (byte) seg.get(4));
		Assert.assertEquals("wrong value", 0b00000_101, (byte) seg.get(5));
		Assert.assertEquals("wrong value", 0b00000_010, (byte) seg.get(6));
		Assert.assertEquals("wrong value", 0b00000_011, (byte) seg.get(7));	
	}
	
	@Test
	public void SegmentationTest2() {
		//startsituation:
		ArrayList<Byte> b = new ArrayList<Byte>();

		b.add( (byte) 0b00110100);
		b.add( (byte) 0b10011011);
		b.add( (byte) 0b01010011);
		b.add( (byte) 0b11111111);
		int segS = 8;
		
		//Aktion:
		ArrayList<Byte> seg = Segmentation.splitIntoSegments(b, segS);
		
		//Endsituation:
		Assert.assertEquals("wrong value", 4, seg.size());
		Assert.assertEquals("wrong value", 0b00110100, (byte) seg.get(0));
		Assert.assertEquals("wrong value", (byte)0b10011011, (byte) seg.get(1));
		Assert.assertEquals("wrong value", 0b01010011, (byte) seg.get(2));
		Assert.assertEquals("wrong value", (byte)0b11111111, (byte) seg.get(3));
	
	}
	
	@Test
	public void SegmentationTest3() {
		//startsituation:
		ArrayList<Byte> b = new ArrayList<Byte>();

		b.add( (byte) 0b00110100);
		b.add( (byte) 0b10011011);
		int segS = 1;
		
		//Aktion:
		ArrayList<Byte> seg = Segmentation.splitIntoSegments(b, segS);
		
		//Endsituation:
		Assert.assertEquals("wrong value", 16, seg.size());
		Assert.assertEquals("wrong value", 0b00000000, (byte) seg.get(0));
		Assert.assertEquals("wrong value", 0b00000000, (byte) seg.get(1));
		Assert.assertEquals("wrong value", 0b00000001, (byte) seg.get(2));
		Assert.assertEquals("wrong value", 0b00000001, (byte) seg.get(3));
		Assert.assertEquals("wrong value", 0b00000000, (byte) seg.get(4));
		Assert.assertEquals("wrong value", 0b00000001, (byte) seg.get(5));
		Assert.assertEquals("wrong value", 0b00000000, (byte) seg.get(6));
		Assert.assertEquals("wrong value", 0b00000000, (byte) seg.get(7));
		Assert.assertEquals("wrong value", 0b00000001, (byte) seg.get(8));
		Assert.assertEquals("wrong value", 0b00000000, (byte) seg.get(9));
		Assert.assertEquals("wrong value", 0b00000000, (byte) seg.get(10));
		Assert.assertEquals("wrong value", 0b00000001, (byte) seg.get(11));
		Assert.assertEquals("wrong value", 0b00000001, (byte) seg.get(12));
		Assert.assertEquals("wrong value", 0b00000000, (byte) seg.get(13));
		Assert.assertEquals("wrong value", 0b00000001, (byte) seg.get(14));
		Assert.assertEquals("wrong value", 0b00000001, (byte) seg.get(15));
	}
	
	@Test
	public void SegmentationTest9() {
		//startsituation:
		ArrayList<Byte> b = new ArrayList<Byte>();

		b.add( (byte) 0b00110100);
		int segS = 2;
		
		//Aktion:
		ArrayList<Byte> seg = Segmentation.splitIntoSegments(b, segS);
		
		//Endsituation:
		Assert.assertNull("not null", seg);

	
	}
	
	@Test
	public void SegmentationTest10() {
		//startsituation:
		ArrayList<Byte> b = new ArrayList<Byte>();

		b.add( (byte) 0b00110100);
		b.add( (byte) 0b00011011);
		b.add( (byte) 0b01010011);
		int segS = 0;
		
		//Aktion:
		ArrayList<Byte> seg = Segmentation.splitIntoSegments(b, segS);
		
		//Endsituation:
		Assert.assertNull("not null", seg);	
	}
	
	@Test
	public void SegmentationTest11() {
		//startsituation:
		ArrayList<Byte> b = new ArrayList<Byte>();

		b.add( (byte) 0b00110100);
		b.add( (byte) 0b00011011);
		b.add( (byte) 0b01010011);
		int segS = 9;
		
		//Aktion:
		ArrayList<Byte> seg = Segmentation.splitIntoSegments(b, segS);
		
		//Endsituation:
		Assert.assertNull("not null", seg);

	}
	
	//
	//
	//   HEADER TESTS
	//
	//
	@Test
	public void HeaderWriteDataContainerTypeTest() {
		//without container-para1, data-para1
		//startsituation:
		Data d = new Text();
		Container c = new ImageContainer();
		Header h = new Header();
		
		d.setHeader(h);
		c.setHeader(d.getHeader());
			
		//Aktion:
		
		h.createHeaderBytes();
		
		//Endsituation: 
		
		byte b[] =h.getBytes();
	
		Assert.assertEquals("wrong value", (byte) 0b0001_0001, b[0]);
		Assert.assertEquals("wrong value", (byte) 0b0000_0001, b[1]);
		Assert.assertEquals("wrong value", (byte) 0b0000_0000, b[2]);
		Assert.assertEquals("wrong value", (byte) 0b0000_0000, b[3]);
		Assert.assertEquals("wrong value", (byte) 0b0000_0000, b[4]);
		Assert.assertEquals("wrong value", (byte) 0b0000_0000, b[5]);

		

	}
	
	@Test
	public void BigComplexTest1() {
		//startsituation:
		Text t = new Text();
		String s = new String("I also did a double-take when I saw a "
				+ "language designer make that quote. There is nothing "
				+ "simpler than an unsigned integer. Signed integers are complicated."
				+ " Particularly when you consider the bit twiddling at the transistor"
				+ " level. And how does a signed integer shift? I had to conclude that"
				+ " the designer of Java has a serious issue understanding boolean logic.");
		String p = new String(s+s+s+s+s+s+s);
		String q = new String(p+p+p+p+p+p+p+p+p+p+p);
		String k = new String(q+q+q+q+q+q+q+q+q+q+q+q); 

		t.setValue(k);
		
		ImageContainer ic = new ImageContainer();
		try {
			ic.setPath("c:\\HiDa\\4.jpg");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ic.setResultPath("c:\\HiDa\\4_6bit.jpg");
		ic.setSegmentSize(8);

		
		//Aktion:

		ic.insert(t);
		//Endsituation:
		
		try {
			BufferedImage inputImage = ImageIO.read(new File(ic.getResultPath()));
			int rgb = inputImage.getRGB(0, 3);
			int blue = 0x0000ff & rgb;

			int green = 0x0000ff & (rgb >> 8);

			int red = 0x0000ff & (rgb >> 16);

		//	Assert.assertEquals("first byte wrong",(byte) 0b0001_0001 ,(byte)blue );
			
			rgb = inputImage.getRGB(0, 4);
			blue = 0x0000ff & rgb;

			green = 0x0000ff & (rgb >> 8);

			red = 0x0000ff & (rgb >> 16);
			
			rgb = inputImage.getRGB(0, 5);
			blue = 0x0000ff & rgb;

			green = 0x0000ff & (rgb >> 8);

			red = 0x0000ff & (rgb >> 16);
			
			rgb = inputImage.getRGB(0, 6);
			blue = 0x0000ff & rgb;

			green = 0x0000ff & (rgb >> 8);

			red = 0x0000ff & (rgb >> 16);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		


	}
	
	
}
